<?php
/*
* Accounts Controller
*/


    // Create or access a Session
     session_start();
    

    // Get the database connections file
    require_once '../library/connections.php';
    // Get the acme model for use as needed
    require_once '../model/acme-model.php';
    // Get the accounts model
    require_once '../model/accounts-model.php';
    // Get the functions library
    require_once '../library/functions.php';

    // Get the array of categories
	$categories = getCategories();
    
    //Store the function to build the navigation bar into a variable called $navList
    $navList = buildNavBar($categories);

    $action = filter_input(INPUT_POST, 'action');
    if ($action == NULL){
        $action = filter_input(INPUT_GET, 'action');
    }

    switch ($action){
        case 'login':
            include '../view/login.php';
            break;

        case 'Register':
            include '../view/registration.php';
            break;
             
        case 'register':
            // Filter and store the data
            $clientFirstname = filter_input(INPUT_POST, 'clientFirstname', FILTER_SANITIZE_STRING);
            $clientLastname = filter_input(INPUT_POST, 'clientLastname', FILTER_SANITIZE_STRING);
            $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
            $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
            
            //Check Email
            $clientEmail = checkEmail($clientEmail);
            //Check Password
            $checkPassword = checkPassword($clientPassword);
            
            
            //Checking for an existing email
            $existingEmail = checkExistingEmail($clientEmail);
            
            // Check for existing email address in the table
            if($existingEmail){
                $message = '<p class="notice">That email address already exists. Do you want to login instead?</p>';
                include '../view/login.php';
                exit;
            }
            
            // Check for missing data
            if (empty($clientFirstname) || empty($clientLastname) || empty($clientEmail) || empty($checkPassword)) {
                $message = '<p class="notice">Please provide information for all empty form fields.</p>';
                include '../view/registration.php';
                exit; 
            }
            
            // Hash the checked password
            $hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);
            
            // Send the data to the model
            $regOutcome = regClient($clientFirstname, $clientLastname, $clientEmail, $hashedPassword);
        
            // Check and report the result
            if($regOutcome === 1){
                setcookie('firstname', $clientFirstname, strtotime('+1 year'), '/');
                $_SESSION['message'] = "<p class='notice'>Thanks for registering $clientFirstname. Please use your email and password to login.</p>";
                header('Location: /acme/accounts/?action=login');
                exit;
            } else {
                $message = "<p class='notice'>Sorry $clientFirstname, but the registration failed. Please try again.</p>";
                include '../view/registration.php';
                exit;
            }
            break;
            
        case 'Login':
            // Filter and store the data
            $clientEmail = filter_input(INPUT_POST, 'clientEmail', FILTER_SANITIZE_EMAIL);
            $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
            
            //Check Email
            $clientEmail = checkEmail($clientEmail);
            //Check Password
            $checkPassword = checkPassword($clientPassword);
            
            // Check for missing data
            if (empty($clientEmail) || empty($checkPassword)) {
                $message = '<p class="notice">Please provide information for all empty form fields.</p>';
                include '../view/login.php';
                exit; 
            }
            
            // A valid password exists, proceed with the login process
            // Query the client data based on the email address
            $clientData = getClient($clientEmail);
            
            // Compare the password just submitted against
            // the hashed password for the matching client
            $hashCheck = password_verify($clientPassword, $clientData['clientPassword']);
            
            // If the hashes don't match create an error
            // and return to the login view
            if(!$hashCheck) {
                $message = '<p class="notice">Please check your password and try again.</p>';
                include '../view/login.php';
                exit;
            }
            
            // A valid user exists, log them in
            $_SESSION['loggedin'] = TRUE;
            
            // Remove the password from the array
            // the array_pop function removes the last
            // element from an array
            array_pop($clientData);
            
            // Store the array into the session
            $_SESSION['clientData'] = $clientData;
            
            // Remove cookie
            setcookie('firstname', '', strtotime('-1 year'), '/');
            
            // Send them to the admin view
            include '../view/admin.php';
            exit;
        
        case 'Logout':        
            // Remove cookie
            setcookie('firstname', '', strtotime('-1 year'), '/');
            
            // Destroy the session
            session_destroy();
            
            include '../index.php';
            break;
            
        case 'clientUpdate':
            //Route to client-update.php
            include '../view/client-update.php';
            break;
            
        case 'updateAccount':
            // Filter and store the data
            $updateFirstname = filter_input(INPUT_POST, 'updateFirstname', FILTER_SANITIZE_STRING);
            $updateLastname = filter_input(INPUT_POST, 'updateLastname', FILTER_SANITIZE_STRING);
            $updateEmail = filter_input(INPUT_POST, 'updateEmail', FILTER_SANITIZE_EMAIL);
            $clientId = filter_input(INPUT_POST, 'clientId', FILTER_SANITIZE_NUMBER_INT);
            
            // Check for missing data
            if (empty($updateFirstname) || empty($updateLastname) || empty($updateEmail)) {
                $message = '<p class="notice">Please provide information for all empty form fields.</p>';
                include '../view/client-update.php';
                exit; 
            }
            
            //Check if email is different than one in session.
            //If so, check to make sure new email does not already exist in clients table.
            if ($_SESSION['clientData']['clientEmail'] !== $updateEmail) {
                //Check Email
                $updateEmail = checkEmail($updateEmail);
                
                //Checking for an existing email
                $existingEmail = checkExistingEmail($updateEmail);

                // Check for existing email address in the table
                if($existingEmail){
                    $message = '<p class="notice">That email address already exists. Please use a different one.</p>';
                    include '../view/client-update.php';
                    exit;
                }
            }
            
            // Send the data to the model
            $updateOutcome = updateClient($updateFirstname, $updateLastname, $updateEmail, $clientId);
            
            //Reference function to get updated client data stored into a variable.
            $updatedClientData = getClientById($clientId);
        
            //Use variable of updated client data to create new session with updated info.
            $_SESSION['clientData'] = $updatedClientData;
                
            // Check and report the result
            if($updateOutcome === 1){
                $message = "<p class='notice'>Thanks for updating your information.</p>";
                $_SESSION['message'] = $message;
                include '../view/admin.php';
                exit;
            } else {
                $message = "<p class='notice'>Sorry, but updating your information failed. Please ensure an input was changed and try again.</p>";
                $_SESSION['message'] = $message;
                include '../view/admin.php';
                exit;
            }
            break;
            
        case 'changePassword':
            $clientPassword = filter_input(INPUT_POST, 'clientPassword', FILTER_SANITIZE_STRING);
            $clientId = filter_input(INPUT_POST, 'clientId', FILTER_SANITIZE_NUMBER_INT);
            
            //Check Password
            $checkPassword = checkPassword($clientPassword);
            
            // Check for missing data
            if (empty($checkPassword)) {
                $message = '<p class="notice">Please provide a new password into the input field and ensure it meets password requirements.</p>';
                include '../view/client-update.php';
                exit;
            }
            
            // Hash the checked password
            $hashedPassword = password_hash($clientPassword, PASSWORD_DEFAULT);
            
            // Send the data to the model
            $passwordOutcome = updateClientPassword($hashedPassword, $clientId);
            
            
            // Check and report the result
            if($passwordOutcome === 1){
                $message = "<p class='notice'>Thanks for updating your password.</p>";
                $_SESSION['message'] = $message;
                include '../view/admin.php';
                exit;
            } else {
                $message = "<p class='notice'>Sorry, but updating your password failed. Please ensure you have met password requirements and then try again.</p>";
                $_SESSION['message'] = $message;
                include '../view/admin.php';
                exit;
            }
            break;
                       
        default:
            if(isset($_SESSION['clientData'])) {
                include '../view/admin.php';   
            } else {
                include '../index.php';
            }
    }



?>