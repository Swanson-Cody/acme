<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $prodInfo['invName'] ?> | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/product-detail.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <?php
                    if (isset($message)) {
                        echo $message;
                    }
                ?>
                <?= $displaySpecificProductInfo, $imageThumbnailDisplay; ?>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; March 13, 2019</p>
            </footer>
        </div>
    </body>
</html>