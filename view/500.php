<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Error | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?php
                        include $_SERVER['DOCUMENT_ROOT'].'/acme/common/nav.php';
                    ?>
                </nav>
            </header>
            <main>
                <h1>Server Error</h1>
                <p>Sorry&#44; the server experienced a problem&#46;</p>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Jan. 23, 2019</p>
            </footer>
        </div>
    </body>
</html>