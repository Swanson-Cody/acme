<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/forms.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>Acme Login</h1>
                <?php
                    if (isset($_SESSION['message'])) {
                        echo $_SESSION['message'];
                    }
                ?>
                <form action="/acme/accounts/" method="post">
                    <fieldset>
                        <legend>&#42;All Fields Required&#42;</legend>
                        <label>
                            <span>Email Address</span><input name="clientEmail" id="clientEmail" type="email" <?php if(isset($clientEmail)){echo "value='$clientEmail'";}  ?> required>
                        </label>
                        <label>
                            <span>Password</span><span class="passwordNotice">&#40;Passwords must be at least 8 characters and contain at least 1 number&#44; 1 capital letter and 1 special character&#46;&#41;</span><input type="password" name="clientPassword" id="clientPassword" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                        </label>
                            <input class="styleBttn" type="submit" value="Login">
                            <input type="hidden" name="action" value="Login">
                    </fieldset>
                </form>
                <p class="bttnNotice">Not a member?</p>
                <a href="/acme/accounts/index.php?action=Register" class="createBttn">Create an Account</a>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Jan. 28, 2019</p>
            </footer>
        </div>
    </body>
</html>