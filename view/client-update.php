<?php
    if(!isset($_SESSION['loggedin'])) {
        header('Location: /acme/');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Client Update | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/forms.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>Manage Account</h1>
                
                <?php if(isset($message)) {
                        echo $message;
                    }
                ?>
                
                <h3>Account Update</h3>
                <form method="post" action="/acme/accounts/">
                    <fieldset>
                        <label>
                            <span>First Name</span><input name="updateFirstname" id="updateFirstname" type="text" <?php if(isset($_SESSION['clientData']['clientFirstname'])){echo "value='{$_SESSION['clientData']['clientFirstname']}'";}  ?> required >
                        </label>
                        <label>
                            <span>Last Name</span><input name="updateLastname" id="updateLastname" type="text" <?php if(isset($_SESSION['clientData']['clientLastname'])){echo "value='{$_SESSION['clientData']['clientLastname']}'";}  ?> required >
                        </label>
                        <label>
                            <span>Email Address</span><input name="updateEmail" id="updateEmail" type="email" <?php if(isset($_SESSION['clientData']['clientEmail'])){echo "value='{$_SESSION['clientData']['clientEmail']}'";} ?> required >
                        </label>
                        <input class="styleBttn" type="submit" value="Update Account">
                        <input type="hidden" name="action" value="updateAccount">
                        <input type="hidden" name="clientId" value="<?php if(isset($_SESSION['clientData']['clientId'])){echo $_SESSION['clientData']['clientId'];} ?>">
                    </fieldset>
                </form>
                
                <h3>Change Password</h3>
                <form method="post" action="/acme/accounts/">
                    <fieldset>
                        <label>
                            <span>New Password</span><span class="passwordNotice">&#40;Passwords must be at least 8 characters and contain at least 1 number&#44; 1 capital letter and 1 special character&#46;&#41;</span><input type="password" name="clientPassword" id="clientPassword" pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                        </label>
                        <input class="styleBttn" type="submit" value="Change Password">
                        <input type="hidden" name="action" value="changePassword">
                        <input type="hidden" name="clientId" value="<?php if(isset($_SESSION['clientData']['clientId'])){echo $_SESSION['clientData']['clientId'];} ?>">
                    </fieldset>
                </form>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; March. 07, 2019</p>
            </footer>
        </div>
    </body>
</html>