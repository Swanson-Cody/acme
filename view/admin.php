<?php
    if(!isset($_SESSION['loggedin'])) {
        header('Location: /acme/');
        exit;
    }
    unset($_SESSION['message']);
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <p class="notice">You are logged in.</p>
                <h1>
                    <?php
                        echo "Hello, {$_SESSION['clientData']['clientFirstname']} {$_SESSION['clientData']["clientLastname"]}";
                    ?>
                </h1>
                <?php
                    if (isset($_SESSION['message'])) {
                        echo $_SESSION['message'];
                    }
                ?>
                <h3>Your personal information&#58;</h3>
                <ul>
                    <?php
                        echo "<li><b>First Name:</b> {$_SESSION['clientData']['clientFirstname']}</li> <li><b>Last Name:</b> {$_SESSION['clientData']['clientLastname']}</li> <li><b>Email:</b> {$_SESSION['clientData']['clientEmail']}</li>";
                    ?>
                </ul>
                <p>
                    <a href='/acme/accounts/index.php?action=clientUpdate'>Update Account Information</a>
                </p>
                <?php
                    if($_SESSION['clientData']{'clientLevel'} > 1) {
                        echo "<h3>Your product and image resources&#58;</h3><p>Use the following link to manage your products and images:</p><a href='/acme/products/'>Product Management</a><br><a href='/acme/uploads/'>Image Management</a>";
                    }
                ?>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Jan. 15, 2019</p>
            </footer>
        </div>
    </body>
</html>