<?php
    if(!isset($_SESSION['loggedin']) || $_SESSION['clientData']['clientLevel'] < 2) {
        header('Location: /acme/');
        exit;
    }
    
    if (isset($_SESSION['message'])) {
        $message = $_SESSION['message'];
    }
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Product Management | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/tables.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>Product Management</h1>
                <p>Welcome to the product management page&#46; Please choose an option below&#58;</p>
                <ul>
                    <li>
                        <a href="/acme/products/index.php?action=newCat">Add a New Category</a>
                    </li>
                    <li>
                        <a href="/acme/products/index.php?action=newProd">Add a New Product</a>
                    </li>
                </ul>
                <?php
                    if (isset($message)) {
                        echo $message;
                    } if (isset($prodList)) {
                        echo $prodList;
                    }
                ?>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Feb. 06, 2019</p>
            </footer>
        </div>
    </body>
</html>
<?php
    unset($_SESSION['message']);
?>