<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/home.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <?php 
                    if(isset($message)) {
                        echo $message;
                    }
                ?>
                <h1>Welcome to Acme&#33;</h1>
                <div class="featuredSection">
                    <div>
                        <?php
                            if(isset($_SESSION['feature'])) {
                                echo '<img src="' . $_SESSION['feature']['invImage'] . '" alt="Image of featured product">';
                            }
                        ?>
                        <p>
                            <?php
                                if(isset($_SESSION['feature'])) {
                                    echo $_SESSION['feature']['invDescription'];
                                }
                            ?>
                        </p>
                        <a href="#">
                            <img src="/acme/images/site/iwantit.gif" alt="Buy button">
                        </a>
                    </div>
                </div>
<!--
                <section class="reviewsSection">
                    <h2>Acme Rocket Reviews</h2>
                    <ul>
                        <li>&quot;I don&#39;t know how I ever caught roadrunners before this&#46;&quot; &#40;4&#47;5&#41;</li>
                        <li>&quot;That thing was fast&#33;&quot; &#40;4&#47;5&#41;</li>
                        <li>&quot;Talk about fast delivery&#46;&quot; &#40;5&#47;5&#41;</li>
                        <li>&quot;I didn&#39;t even have to pull the meat apart&#46;&quot; &#40;4&#46;5&#47;5&#41;</li>
                        <li>&quot;I&#39;m on my thirtieth one&#46; I love these things&#33;&quot; &#40;5&#47;5&#41;</li>
                    </ul>
                </section>
-->
                <h2 class="lessGap">Featured Recipes</h2>
                <div class="foodSection">
                    <figure>
                        <img src="/acme/images/recipes/bbqsand.jpg" alt="BBQ Sandwich">
                        <figcaption><a href="#">Pulled Roadrunner BBQ</a></figcaption>
                    </figure>
                    <figure>
                        <img src="/acme/images/recipes/potpie.jpg" alt="Pot Pie">
                        <figcaption><a href="#">Roadrunner Pot Pie</a></figcaption>
                    </figure>
                    <figure>
                        <img src="/acme/images/recipes/soup.jpg" alt="Soup">
                        <figcaption><a href="#">Roadrunner Soup</a></figcaption>
                    </figure>
                    <figure>
                        <img src="/acme/images/recipes/taco.jpg" alt="Tacos">
                        <figcaption><a href="#">Roadrunner Tacos</a></figcaption>
                    </figure>
                </div>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Jan. 15, 2019</p>
            </footer>
        </div>
    </body>
</html>