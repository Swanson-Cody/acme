<?php
    if(!isset($_SESSION['loggedin']) || $_SESSION['clientData']['clientLevel'] < 2) {
        header('Location: /acme/');
        exit;
    }

    //Build a drop-down list using the $categories array
    $catList = '<select name="categoryId" id="categoryId">';
    $catList .= '<option value="none" selected disabled>Choose a Category</option>';
    foreach ($categories as $category) {
        $catList .= "<option value='$category[categoryId]'";
        if(isset($categoryId)) {
            if($category['categoryId'] === $categoryId) {
                $catList .= ' selected ';
            }
        } elseif(isset($prodInfo['categoryId'])) {
            if($category['categoryId'] === $prodInfo['categoryId']) {
                $catList .= ' selected ';
            }
        }
        $catList .= ">$category[categoryName]</option>";
    }
    $catList .= '</select>';
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php
                if(isset($prodInfo['invName'])){ 
                    echo "Modify $prodInfo[invName] ";
                } elseif(isset($invName)) {
                    echo $invName;
                }
            ?> | Acme, Inc.
        </title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/forms.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>
                    <?php 
                        if(isset($prodInfo['invName'])) {
                            echo "Modify $prodInfo[invName] ";
                        } elseif(isset($invName)) {
                            echo $invName;
                        }
                    ?>
                </h1>
                <?php
                    if (isset($message)) {
                        echo $message;
                    }
                ?>
                <p>Modify a product below&#46; All fields are required&#33;</p>
                <form action="/acme/products/" method="post">
                    <fieldset>
                        <?= $catList; ?>
                        <label>
                            <span>Product Name</span><input type="text" name="invName" id="invName" <?php if(isset($invName)){echo "value='$invName'";} elseif(isset($prodInfo['invName'])) {echo "value='$prodInfo[invName]'"; }  ?> required>
                        </label>
                        <label>
                            <span>Product Description</span><input type="text" name="invDescription" id="invDescription" <?php if(isset($invDescription)){echo "value='$invDescription'";} elseif(isset($prodInfo['invDescription'])) {echo "value='$prodInfo[invDescription]'"; } ?> required>
                        </label>
                        <label>
                            <span>Product Image &#40;path to image&#41;</span><input type="text" name="invImage" id="invImage" <?php if(isset($invImage)){echo "value='$invImage'";} elseif(isset($prodInfo['invImage'])) {echo "value='$prodInfo[invImage]'"; } ?> required>
                        </label>
                        <label>
                            <span>Product Thumbnail &#40;path to thumbnail&#41;</span><input type="text" name="invThumbnail" id="invThumbnail" <?php if(isset($invThumbnail)){echo "value='$invThumbnail'";} elseif(isset($prodInfo['invThumbnail'])) {echo "value='$prodInfo[invThumbnail]'"; } ?> required>
                        </label>
                        <label>
                            <span>Product Price</span><input type="number" name="invPrice" id="invPrice" <?php if(isset($invPrice)){echo "value='$invPrice'";} elseif(isset($prodInfo['invPrice'])) {echo "value='$prodInfo[invPrice]'"; } ?> required>
                        </label>
                        <label>
                            <span>Amount in Stock</span><input type="number" name="invStock" id="invStock" <?php if(isset($invStock)){echo "value='$invStock'";} elseif(isset($prodInfo['invStock'])) {echo "value='$prodInfo[invStock]'"; } ?> required>
                        </label>
                        <label>
                            <span>Product Size</span><input type="number" name="invSize" id="invSize" <?php if(isset($invSize)){echo "value='$invSize'";} elseif(isset($prodInfo['invSize'])) {echo "value='$prodInfo[invSize]'"; } ?> required>
                        </label>
                        <label>
                            <span>Product Weight</span><input type="number" name="invWeight" id="invWeight" <?php if(isset($invWeight)){echo "value='$invWeight'";} elseif(isset($prodInfo['invWeight'])) {echo "value='$prodInfo[invWeight]'"; } ?> required>
                        </label>
                        <label>
                            <span>Location</span><input type="text" name="invLocation" id="invLocation" <?php if(isset($invLocation)){echo "value='$invLocation'";} elseif(isset($prodInfo['invLocation'])) {echo "value='$prodInfo[invLocation]'"; } ?> required>
                        </label>
                        <label>
                            <span>Vendor Name</span><input type="text" name="invVendor" id="invVendor" <?php if(isset($invVendor)){echo "value='$invVendor'";} elseif(isset($prodInfo['invVendor'])) {echo "value='$prodInfo[invVendor]'"; } ?> required>
                        </label>
                        <label>
                            <span>Primary Material</span><input type="text" name="invStyle" id="invStyle" <?php if(isset($invStyle)){echo "value='$invStyle'";} elseif(isset($prodInfo['invStyle'])) {echo "value='$prodInfo[invStyle]'"; } ?> required>
                        </label>
                        <input class="styleBttn" type="submit" value="Update Product">
                        
                        <input type="hidden" name="action" value="updateProd">
                        
                        <input type="hidden" name="invId" value="<?php if(isset($prodInfo['invId'])){ echo $prodInfo['invId'];} elseif(isset($invId)){ echo $invId; } ?>">
                    </fieldset>
                </form>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; March 06, 2019</p>
            </footer>
        </div>
    </body>
</html>