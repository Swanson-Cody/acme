<?php
    if(!isset($_SESSION['loggedin']) || $_SESSION['clientData']['clientLevel'] < 2) {
        header('Location: /acme/');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Create Category| Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/forms.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>Add Category</h1>
                <?php
                    if (isset($message)) {
                        echo $message;
                    }
                ?>
                <p>Add a new category of products below&#46;</p>
                <form action="/acme/products/" method="post">
                    <fieldset>
                        <label>
                            <span>New Category Name</span><input type="text" name="categoryName" id="categoryName" required>
                        </label>
                        <input class="styleBttn" type="submit" value="Add Category">
                        <input type="hidden" name="action" value="addCat">
                    </fieldset>
                </form>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Feb. 06, 2019</p>
            </footer>
        </div>
    </body>
</html>