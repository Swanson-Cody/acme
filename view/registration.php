<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registration | Acme, Inc.</title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/forms.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>Acme Registration</h1>
                <?php
                    if (isset($message)) {
                        echo $message;
                    }
                ?>
                <form action="/acme/accounts/index.php" method="post">
                    <fieldset>
                        <legend>&#42;All Fields Required&#42;</legend>
                        <label>
                            <span>First Name</span><input name="clientFirstname" id="clientFirstname" type="text" <?php if(isset($clientFirstname)){echo "value='$clientFirstname'";}  ?> required>
                        </label>
                        <label>
                            <span>Last Name</span><input name="clientLastname" id="clientLastname" type="text" <?php if(isset($clientLastname)){echo "value='$clientLastname'";}  ?> required>
                        </label>
                        <label>
                            <span>Email Address</span><input name="clientEmail" id="clientEmail" type="email" <?php if(isset($clientEmail)){echo "value='$clientEmail'";}  ?> required>
                        </label>
                        <label>
                            <span>Password</span><span class="passwordNotice">&#40;Passwords must be at least 8 characters and contain at least 1 number&#44; 1 capital letter and 1 special character&#46;&#41;</span><input type="password" name="clientPassword" id="clientPassword" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                        </label>
                        <input class="styleBttn" type="submit" value="Register">
                        <!-- Add the action name - value pair -->
                        <input type="hidden" name="action" value="register">
                    </fieldset>
                </form>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; Jan. 28, 2019</p>
            </footer>
        </div>
    </body>
</html>