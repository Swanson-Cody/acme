<?php
    if(!isset($_SESSION['loggedin']) || $_SESSION['clientData']['clientLevel'] < 2) {
        header('Location: /acme/');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php
                if(isset($prodInfo['invName'])){ 
                    echo "Delete $prodInfo[invName] ";
                } elseif(isset($invName)) {
                    echo $invName;
                }
            ?> | Acme, Inc.
        </title>
        <link rel="stylesheet" media="screen" href="/acme/css/template.css">
        <link rel="stylesheet" media="screen" href="/acme/css/forms.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/header.php';
                ?>
                <nav>
                    <?= $navList; ?>
                </nav>
            </header>
            <main>
                <h1>
                    <?php 
                        if(isset($prodInfo['invName'])) {
                            echo "Delete $prodInfo[invName] ";
                        } elseif(isset($invName)) {
                            echo $invName;
                        }
                    ?>
                </h1>
                <?php
                    if (isset($message)) {
                        echo $message;
                    }
                ?>
                <p>Confirm product deletion&#46; &#40;Once deleted it will be permanently gone&#46;&#41;</p>
                <form action="/acme/products/" method="post">
                    <fieldset>
                        <label>
                            <span>Product Name</span><input type="text" name="invName" id="invName" <?php if(isset($prodInfo['invName'])){echo "value='$prodInfo[invName]'";} ?> readonly >
                        </label>
                        <label>
                            <span>Product Description</span><input type="text" name="invDescription" id="invDescription" <?php if(isset($prodInfo['invDescription'])){echo "value='$prodInfo[invDescription]'";} ?> readonly >
                        </label>
                        
                        <input class="styleBttn" type="submit" value="Delete Product">
                        
                        <input type="hidden" name="action" value="deleteProd">
                        
                        <input type="hidden" name="invId" value="<?php if(isset($prodInfo['invId'])){ echo $prodInfo['invId'];} ?>">
                    </fieldset>
                </form>
            </main>
            <footer>
                <?php
                    include $_SERVER['DOCUMENT_ROOT'].'/acme/common/footer.php';
                ?>
                <p>Last updated&#58; March 06, 2019</p>
            </footer>
        </div>
    </body>
</html>