<?php


    function insertProduct($invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $categoryId, $invVendor, $invStyle) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();
        
        // The SQL statement
        $sql = 'INSERT INTO inventory (invName, invDescription, invImage, invThumbnail, invPrice, invStock, invSize, invWeight, invLocation, categoryId, invVendor, invStyle)
         VALUES (:invName, :invDescription, :invImage, :invThumbnail, :invPrice, :invStock, :invSize, :invWeight, :invLocation, :categoryId, :invVendor, :invStyle)';
        
        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);
        
        // The next 12 lines replace the placeholders in the SQL
        // statement with the actual values in the variables
        // and tells the database the type of data it is
         $stmt->bindValue(':invName', $invName, PDO::PARAM_STR);
         $stmt->bindValue(':invDescription', $invDescription, PDO::PARAM_STR);
         $stmt->bindValue(':invImage', $invImage, PDO::PARAM_STR);
         $stmt->bindValue(':invThumbnail', $invThumbnail, PDO::PARAM_STR);
         $stmt->bindValue(':invPrice', $invPrice, PDO::PARAM_STR);
         $stmt->bindValue(':invStock', $invStock, PDO::PARAM_INT);
         $stmt->bindValue(':invSize', $invSize, PDO::PARAM_INT);
         $stmt->bindValue(':invWeight', $invWeight, PDO::PARAM_INT);
         $stmt->bindValue(':invLocation', $invLocation, PDO::PARAM_STR);
         $stmt->bindValue(':categoryId', $categoryId, PDO::PARAM_INT);
         $stmt->bindValue(':invVendor', $invVendor, PDO::PARAM_STR);
         $stmt->bindValue(':invStyle', $invStyle, PDO::PARAM_STR);
        
        // Insert the data
        $stmt->execute();
        
        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();
        
        // Close the database interaction
        $stmt->closeCursor();
        
        // Return the indication of success (rows changed)
        return $rowsChanged;
    }



    //Function will get basic product information from the inventory table for starting an update or delete process
    function getProductBasics() {
        $database = acmeConnect();
        $sql = 'SELECT invName, invId FROM inventory ORDER BY invName ASC';
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $products;
    }



    // Get product information by invId
    function getProductInfo($invId){
        $database = acmeConnect();
        $sql = 'SELECT * FROM inventory WHERE invId = :invId';
        $stmt = $database->prepare($sql);
        $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
        $stmt->execute();
        $prodInfo = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $prodInfo;
    }



    //Update a product
    function updateProduct($invId, $invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $categoryId, $invVendor, $invStyle) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();

        // The SQL statement
        $sql = 'UPDATE inventory SET invName = :invName, invDescription = :invDescription, invImage = :invImage, invThumbnail = :invThumbnail, invPrice = :invPrice, invStock = :invStock, invSize = :invSize, invWeight = :invWeight, invLocation = :invLocation, categoryId = :categoryId, invVendor = :invVendor, invStyle = :invStyle WHERE invId = :invId';

        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);

        // The next 12 lines replace the placeholders in the SQL
        // statement with the actual values in the variables
        // and tells the database the type of data it is
        $stmt->bindValue(':invName', $invName, PDO::PARAM_STR);
        $stmt->bindValue(':invDescription', $invDescription, PDO::PARAM_STR);
        $stmt->bindValue(':invImage', $invImage, PDO::PARAM_STR);
        $stmt->bindValue(':invThumbnail', $invThumbnail, PDO::PARAM_STR);
        $stmt->bindValue(':invPrice', $invPrice, PDO::PARAM_STR);
        $stmt->bindValue(':invStock', $invStock, PDO::PARAM_INT);
        $stmt->bindValue(':invSize', $invSize, PDO::PARAM_INT);
        $stmt->bindValue(':invWeight', $invWeight, PDO::PARAM_INT);
        $stmt->bindValue(':invLocation', $invLocation, PDO::PARAM_STR);
        $stmt->bindValue(':categoryId', $categoryId, PDO::PARAM_INT);
        $stmt->bindValue(':invVendor', $invVendor, PDO::PARAM_STR);
        $stmt->bindValue(':invStyle', $invStyle, PDO::PARAM_STR);
        $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);

        // Insert the data
        $stmt->execute();

        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();

        // Close the database interaction
        $stmt->closeCursor();

        // Return the indication of success (rows changed)
        return $rowsChanged;
    }



    //Delete a product
    function deleteProduct($invId) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();

        // The SQL statement
        $sql = 'DELETE FROM inventory WHERE invId = :invId';

        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);

        // The next line replaces the placeholder in the SQL
        // statement with the actual value in the variable
        // and tells the database the type of data it is
        $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);

        // Insert the data
        $stmt->execute();

        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();

        // Close the database interaction
        $stmt->closeCursor();

        // Return the indication of success (rows changed)
        return $rowsChanged;
    }

    // Get a list of products based on the category
    function getProductsByCategory($categoryName){
        $database = acmeConnect();
        $sql = 'SELECT * FROM inventory WHERE categoryId IN (SELECT categoryId FROM categories WHERE categoryName = :categoryName)';
        $stmt = $database->prepare($sql);
        $stmt->bindValue(':categoryName', $categoryName, PDO::PARAM_STR);
        $stmt->execute();
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $products;
    }

    // Select featured/product that is not null
    function getFeatured() {
        $database = acmeConnect();
        $sql = 'SELECT invName, invDescription, invImage FROM inventory WHERE invFeatured = 1';
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $featureInfo = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $featureInfo;
    }

    // Clear featured item with status of null before setting to true for new featured item via invId.
    function setFeatured($invId) {
        $database = acmeConnect();
        $sql = 'UPDATE inventory SET invFeatured = NULL WHERE invFeatured = 1';
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        
        $database = acmeConnect();
        $sql = 'UPDATE inventory SET invFeatured = 1 WHERE invId = :invId';
        $stmt = $database->prepare($sql);
        $stmt->bindValue(':invId', $invId, PDO::PARAM_INT);
        $stmt->execute();
        $featureResult = $stmt->rowCount();
        $stmt->closeCursor();
        return $featureResult;
    }

?>