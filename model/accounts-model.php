<?php
// Accounts model for site visitors


    //Will handle site registrations
    function regClient($clientFirstname, $clientLastname, $clientEmail, $clientPassword) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();
        
        // The SQL statement
        $sql = 'INSERT INTO clients (clientFirstname, clientLastname,clientEmail, clientPassword)
         VALUES (:clientFirstname, :clientLastname, :clientEmail, :clientPassword)';
        
        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);
        
        // The next set of lines replace the placeholders in the SQL
        // statement with the actual values in the variables
        // and tells the database the type of data it is
        $stmt->bindValue(':clientFirstname', $clientFirstname, PDO::PARAM_STR);
        $stmt->bindValue(':clientLastname', $clientLastname, PDO::PARAM_STR);
        $stmt->bindValue(':clientEmail', $clientEmail, PDO::PARAM_STR);
        $stmt->bindValue(':clientPassword', $clientPassword, PDO::PARAM_STR);
        
        // Insert the data
        $stmt->execute();
        
        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();
        
        // Close the database interaction
        $stmt->closeCursor();
        
        // Return the indication of success (rows changed)
        return $rowsChanged;
    }


    //Check for an existing email address
    function checkExistingEmail($clientEmail) {
        $database = acmeConnect();
        $sql = 'SELECT clientEmail FROM clients WHERE clientEmail = :email';
        $stmt = $database->prepare($sql);
        $stmt->bindValue(':email', $clientEmail, PDO::PARAM_STR);
        $stmt->execute();
        $matchEmail = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if(empty($matchEmail)){
            return 0;
        } else {
            return 1;
        }
    }


    // Get client data based on an email address
    function getClient($clientEmail) {
        $database = acmeConnect();
        $sql = 'SELECT clientId, clientFirstname, clientLastname, clientEmail, clientLevel, clientPassword 
             FROM clients
             WHERE clientEmail = :email';
        $stmt = $database->prepare($sql);
        $stmt->bindValue(':email', $clientEmail, PDO::PARAM_STR);
        $stmt->execute();
        $clientData = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $clientData;
    }


    // Update client information in database
    function updateClient($updateFirstname, $updateLastname, $updateEmail, $clientId) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();

        // The SQL statement
        $sql = 'UPDATE clients SET clientFirstname = :updateFirstname, clientLastname = :updateLastname, clientEmail = :updateEmail WHERE clientId = :clientId';

        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);

        // The next set of lines replaces the placeholders in the SQL
        // statement with the actual values in the variables
        // and tells the database the type of data it is
        $stmt->bindValue(':updateFirstname', $updateFirstname, PDO::PARAM_STR);
        $stmt->bindValue(':updateLastname', $updateLastname, PDO::PARAM_STR);
        $stmt->bindValue(':updateEmail', $updateEmail, PDO::PARAM_STR);
        $stmt->bindvalue(':clientId', $clientId, PDO::PARAM_INT);

        // Insert the data
        $stmt->execute();

        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();

        // Close the database interaction
        $stmt->closeCursor();

        // Return the indication of success (rows changed)
        return $rowsChanged;
    }


    // Retrieve updated client information in database based on clientId
    function getClientById($clientId) {
        $database = acmeConnect();
        $sql = 'SELECT clientId, clientFirstname, clientLastname, clientEmail, clientLevel, clientPassword 
             FROM clients
             WHERE clientId = :clientId';
        $stmt = $database->prepare($sql);
        $stmt->bindValue(':clientId', $clientId, PDO::PARAM_INT);
        $stmt->execute();
        $clientData = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $clientData;
    }

    // Update client password in database as a hash
    function updateClientPassword($hashedPassword, $clientId) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();

        // The SQL statement
        $sql = 'UPDATE clients SET clientPassword = :newPassword WHERE clientId = :clientId';

        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);

        // The next set of lines replaces the placeholders in the SQL
        // statement with the actual values in the variables
        // and tells the database the type of data it is
        $stmt->bindValue(':newPassword', $hashedPassword, PDO::PARAM_STR);
        $stmt->bindvalue(':clientId', $clientId, PDO::PARAM_INT);

        // Insert the data
        $stmt->execute();

        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();

        // Close the database interaction
        $stmt->closeCursor();

        // Return the indication of success (rows changed)
        return $rowsChanged;
    }


?>