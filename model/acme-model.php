<?php
    function getCategories(){
        // Create a connection object from the acme connection function
        $database = acmeConnect(); 
        // The SQL statement to be used with the database 
        $sql = 'SELECT categoryId, categoryName FROM categories ORDER BY categoryName ASC'; 
        // The next line creates the prepared statement using the acme connection      
        $stmt = $database->prepare($sql);
        // The next line runs the prepared statement 
        $stmt->execute(); 
        // The next line gets the data from the database and 
        // stores it as an array in the $categories variable 
        $categories = $stmt->fetchAll(); 
        // The next line closes the interaction with the database 
        $stmt->closeCursor(); 
        // The next line sends the array of data back to where the function 
        // was called (this should be the controller) 
        return $categories;
    }

    function insertCategory($categoryName) {
        // Create a connection object using the acme connection function
        $database = acmeConnect();
        
        // The SQL statement
        $sql = 'INSERT INTO categories (categoryName)
            VALUES (:categoryName)';
        
        // Create the prepared statement using the acme connection
        $stmt = $database->prepare($sql);
        
        // The next line replaces the placeholders in the SQL
        // statement with the actual values in the variables
        // and tells the database the type of data it is
        $stmt->bindValue(':categoryName', $categoryName, PDO::PARAM_STR);
        
        // Insert the data
        $stmt->execute();
        
        // Ask how many rows changed as a result of our insert
        $rowsChanged = $stmt->rowCount();
        
        // Close the database interaction
        $stmt->closeCursor();
        
        // Return the indication of success (rows changed)
        return $rowsChanged;
    }

?>