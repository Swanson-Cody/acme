<div class="headerTop">
    <a href="/acme/"><img class="logoImg" src="/acme/images/site/logo.gif" alt="The ACME logo with link to home page"></a>
    <p>
        <?php
            if(isset($cookieFirstname)){
                echo "<span class='nameWelcome'>Welcome: $cookieFirstname</span>";
            }
        ?>
        <?php
            if(isset($_SESSION['clientData'])){ 
                echo "<span class='nameWelcome'>Welcome: {$_SESSION['clientData']['clientFirstname']}</span> <a class='adminLink' title='Admin Page' href='/acme/accounts/'>Admin Page</a>";
            }
        ?>
        <a href='/acme/accounts/index.php?action=<?=isset($_SESSION['clientData']) ? 'Logout' : 'login'?>' title='My Account'>
            <img class='accountImg' src='/acme/images/site/account.gif' alt='An image of a red folder'>
            <?=isset($_SESSION['clientData']) ? '<span class="logout">Logout</span>' : 'My Account'?>
        </a>
    </p>
</div>