<?php
/*
* Acme Controller
*/
    

    // Create or access a Session
     session_start();
    

    // Get the database connections file
    require_once 'library/connections.php';
    // Get the acme model for use as needed
    require_once 'model/acme-model.php';
    // Get the functions library
    require_once 'library/functions.php';
    // Get products model
    require_once 'model/products-model.php';

    // Get the array of categories
	$categories = getCategories();
    
    //Store the function to build the navigation bar into a variable called $navList
    $navList = buildNavBar($categories);

    // Check if the firstname cookie exists, get its value
    if(isset($_COOKIE['firstname'])){
        $cookieFirstname = filter_input(INPUT_COOKIE, 'firstname', FILTER_SANITIZE_STRING);
    }

    $_SESSION['feature'] = getFeatured();

    $action = filter_input(INPUT_POST, 'action');
    if ($action == NULL){
        $action = filter_input(INPUT_GET, 'action');
    }

    switch ($action){
        case 'something':
        
        break;

        default:
            include 'view/home.php';
            break;
    }



?>