<?php

// Products Controller


    // Create or access a Session
     session_start();


    // Get the database connections file
    require_once '../library/connections.php';
    // Get the acme model for use as needed
    require_once '../model/acme-model.php';
    // Get the products model
    require_once '../model/products-model.php';
    // Get the functions library
    require_once '../library/functions.php';
    //Get the uploads model
    require_once '../model/uploads-model.php';
    


    // Get the array of categories
	$categories = getCategories();


    $action = filter_input(INPUT_POST, 'action');
    if ($action == NULL){
        $action = filter_input(INPUT_GET, 'action');
    }

    //Store the function to build the navigation bar into a variable called $navList
    $navList = buildNavBar($categories);


    switch ($action){
        case 'newCat':
            include '../view/new-cat.php';
            break;
            
        case 'newProd':
            include '../view/new-prod.php';
            break;
            
        case 'addCat':
            $categoryName = filter_input(INPUT_POST, 'categoryName');
            
            // Check for missing data
            if(empty($categoryName)){
                $message = '<p class="notice">Please provide information for all empty form fields.</p>';
                include '../view/new-cat.php';
                exit; 
            }
            
            // Send the data to the model
            $categoryOutcome = insertCategory($categoryName);
            
            // Check and report the result
            if($categoryOutcome === 1){
                header('Location: /acme/products/');
                exit;
            } else {
                $message = "<p class='notice'>Sorry, but adding the category '$categoryName' failed. Please try again.</p>";
                include '../view/new-cat.php';
                exit;
            }
            
            break;
            
        case 'addProd':
            // Filter and store the data
            $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
            $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
            $invImage = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_STRING);
            $invThumbnail = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_STRING);
            $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
            $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
            $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_INT);
            $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
            $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
            $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
            $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);
            
            $invPrice = checkInvPrice($invPrice);
            $invStock = checkInvStock($invStock);
            $invSize = checkInvSize($invSize);
            $invWeight = checkInvWeight($invWeight);
            
             // Check for missing data
            if(empty($invName) || empty($invDescription) || empty($invImage) || empty($invThumbnail) || empty($invPrice) || empty($invStock) || empty($invSize) || empty($invWeight) || empty($invLocation) || empty($categoryId) || empty($invVendor) || empty($invStyle)){
                $message = '<p class="notice">Please provide information for all empty form fields.</p>';
                include '../view/new-prod.php';
                exit;
            }
            
            // Send the data to the model
            $ProductOutcome = insertProduct($invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $categoryId, $invVendor, $invStyle);
        
            // Check and report the result
            if($ProductOutcome === 1){
                $message = "<p class='notice'>You have successfully added $invName.</p>";
                include '../view/new-prod.php';
                exit;
            } else {
                $message = "<p class='notice'>Sorry, but adding the product '$invName' failed. Please try again.</p>";
                include '../view/new-prod.php';
                exit;
            }
            
            break;
            
        case 'mod':
            $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            $prodInfo = getProductInfo($invId);
            if(count($prodInfo)<1){
                $message = "<p class='notice'>Sorry, no product information could be found.</p>";
            }
            include '../view/prod-update.php';
            exit;
            break;
        
        case 'updateProd':
            // Filter and store the data
            $invId = filter_input(INPUT_POST, 'invId', FILTER_SANITIZE_NUMBER_INT);
            $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);
            $invDescription = filter_input(INPUT_POST, 'invDescription', FILTER_SANITIZE_STRING);
            $invImage = filter_input(INPUT_POST, 'invImage', FILTER_SANITIZE_STRING);
            $invThumbnail = filter_input(INPUT_POST, 'invThumbnail', FILTER_SANITIZE_STRING);
            $invPrice = filter_input(INPUT_POST, 'invPrice', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $invStock = filter_input(INPUT_POST, 'invStock', FILTER_SANITIZE_NUMBER_INT);
            $invSize = filter_input(INPUT_POST, 'invSize', FILTER_SANITIZE_NUMBER_INT);
            $invWeight = filter_input(INPUT_POST, 'invWeight', FILTER_SANITIZE_NUMBER_INT);
            $invLocation = filter_input(INPUT_POST, 'invLocation', FILTER_SANITIZE_STRING);
            $categoryId = filter_input(INPUT_POST, 'categoryId', FILTER_SANITIZE_NUMBER_INT);
            $invVendor = filter_input(INPUT_POST, 'invVendor', FILTER_SANITIZE_STRING);
            $invStyle = filter_input(INPUT_POST, 'invStyle', FILTER_SANITIZE_STRING);
            
            $invPrice = checkInvPrice($invPrice);
            $invStock = checkInvStock($invStock);
            $invSize = checkInvSize($invSize);
            $invWeight = checkInvWeight($invWeight);
            
             // Check for missing data
            if(empty($invName) || empty($invDescription) || empty($invImage) || empty($invThumbnail) || empty($invPrice) || empty($invStock) || empty($invSize) || empty($invWeight) || empty($invLocation) || empty($categoryId) || empty($invVendor) || empty($invStyle)){
                $message = '<p class="notice">Please provide information for all empty form fields.</p>';
                include '../view/prod-update.php';
                exit;
            }
            
            // Send the data to the model
            $updateResult = updateProduct($invId, $invName, $invDescription, $invImage, $invThumbnail, $invPrice, $invStock, $invSize, $invWeight, $invLocation, $categoryId, $invVendor, $invStyle);
        
            // Check and report the result
            if($updateResult === 1){
                $message = "<p class='notice'>You have successfully updated $invName.</p>";
                $_SESSION['message'] = $message;
                header('location: /acme/products/');
                exit;
            } else {
                $message = "<p class='notice'>Sorry, but updating the product '$invName' failed. Please try again.</p>";
                include '../view/prod-update.php';
                exit;
            }
            break;
            
        case 'del':
            $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            $prodInfo = getProductInfo($invId);
            if(count($prodInfo)<1){
                $message = "<p class='notice'>Sorry, no product information could be found.</p>";
            }
            include '../view/prod-delete.php';
            exit;
            break;
            
        case 'deleteProd':
            // Filter and store the data
            $invId = filter_input(INPUT_POST, 'invId', FILTER_SANITIZE_NUMBER_INT);
            $invName = filter_input(INPUT_POST, 'invName', FILTER_SANITIZE_STRING);     
            
            // Send the data to the model
            $deleteResult = deleteProduct($invId);
        
            // Check and report the result
            if($deleteResult === 1){
                $message = "<p class='notice'>You have successfully deleted $invName.</p>";
                $_SESSION['message'] = $message;
                header('location: /acme/products/');
                exit;
            } else {
                $message = "<p class='notice'>Sorry, but deleting the product '$invName' failed. Please try again.</p>";
                $_SESSION['message'] = $message;
                header('location: /acme/products/');
                exit;
            }
            break;
            
        case 'category':
            $categoryName = filter_input(INPUT_GET, 'categoryName', FILTER_SANITIZE_STRING);
            $products = getProductsByCategory($categoryName);
            if(!count($products)){
                $message = "<p class='notice'>Sorry, no $categoryName products could be found.</p>";
            } else {
                $prodDisplay = buildProductsDisplay($products);
            }
            include '../view/category.php';
            break;
            
        case 'displayProd':
            $invId = filter_input(INPUT_GET, 'productId', FILTER_VALIDATE_INT);
            $prodInfo = getProductInfo($invId);
            if($prodInfo == 0) {
                $message = "<p class='notice'>Sorry, no product information could be found.</p>";
                include '../view/home.php';
                exit;
            }
            
            $displaySpecificProductInfo = buildProductDetailDisplay($prodInfo);
            
            // Get thumbnail images from database via function in uploads-model
            $imageArray = getImageThumbnail($invId);
            
            //Custom function for image thumbnail display
            $imageThumbnailDisplay = buildThumbnailDisplay($imageArray);
            
            include '../view/product-detail.php';
            break;
            
        case 'feature':
            $invId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            
            $_SESSION['message'] = '<p class="notice">Last featured product: ' . getFeatured()['invName'] . '</p>';
            
            $newFeaturedSet = setFeatured($invId);
            
            $_SESSION['message'] .= '<p class="notice">Current featured product: ' . getFeatured()['invName'] . '</p>';
            
            header('location: /acme/products/');
            exit;
            break;
            
        default:
            $products = getProductBasics();
            if(count($products) > 0){
                $prodList = '<table>';
                $prodList .= '<thead>';
                $prodList .= '<tr><th>Product Name</th><td>&nbsp;</td><td>&nbsp;</td></tr>';
                $prodList .= '</thead>';
                $prodList .= '<tbody>';
                foreach ($products as $product) {
                    $prodList .= "<tr><td>$product[invName]</td>";
                    $prodList .= "<td><a href='/acme/products?action=mod&id=$product[invId]' title='Click to modify'>Modify</a></td>";
                    $prodList .= "<td><a href='/acme/products?action=del&id=$product[invId]' title='Click to delete'>Delete</a></td>";
                    $prodList .= "<td><a href='/acme/products?action=feature&id=$product[invId]' title='Click to feature'>Feature</a></td></tr>";
                }
                $prodList .= '</tbody></table>';
            } else {
                $message = '<p class="notice">Sorry, no products were returned.</p>';
            }

            
            include '../view/prod-mgmt.php';
            break;
    }

?>